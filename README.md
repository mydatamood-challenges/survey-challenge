# Survey System Challenge
At MyDataMood, we are engaged in understanding consumer trends, and how users can monetize their interests and needs.  
On that purpose, we would like to build a Survey System, that can help us collect user interests in certain products and categories of the economy, and that provides insights about the popularity of each of them.  
For the sake of this challenge we ask you to build the core of this Survey System, considering the following **requirements**:
* This system will feed from survey data. This data will be provided via API (more details below).
* It should store the feeded data in the most appropriate way, so it can be easily queried afterwards.  
  NOTE: we will not evaluate the integration with existing databases. Consider using an in-memory data store, to simulate the presence of an underlying database.
* It should be able to provide insights about the interest of users in products. Thus, it should provide an API endpoint responding to the following queries:
  * What is the most wanted category
  * What is the most wanted product by category

## API
To simplify the challenge and remove language restrictions, this Survey System must provide a REST API which will be used to interact with it.

This API must comply with the following contract:

### PUT /products
Loads the list of available products into the system and removes all previous data (products and interests). This method may be called more than once during the life cycle of the service.

For each product we will store:
* **Id**: unique identifier of the product.
* **Name**: name of the product
* **Category**: The category of the product. Textual symbol representing the type of business a product is enclosed in (i.e. "car_insurances", "landline_internet", "house_mortage"...). You can consider here any category you can think of, we will not evaluate that. But the ones you define should be homogeneous.

**Body**: The list of products to load.  
**Content Type**: application/json

Sample Request:
```json
[
    {
        "id": "1234abcd",
        "name": "Car Insurance for any damage",
        "category": "car_insurances"
    },
    {
        "id": "5678efgh",
        "name": "Internet Cable Connection, up to 600Mb",
        "category": "landline_internet"
    }
]
```

### POST /interest
This endpoint will serve as the main entry point for the results of the surveys.  
It will be used to load the interest shown by a user into a particular product, based on a score between 0 and 10. Being 0 _not interested at all_, and 10 _very much interested_.  
A user cannot score the same product twice.

**Body**: The _Interest_ resource  
**Content Type**: application/json

Sample Request:
```json
{
    "user_id": "asdfasdf123",
    "product_id": "1234abcd",
    "score": 7
}
```

### GET /categories
Endpoint to query category data. It receives three parameters:
* **q**: specifies the type of query to be run. For this exercise it only supports the value "score". It means we want the endpoint to return a list of categories, alongside their score, based on the mean of all the scores of all their products.
* **limit**: The limit in the number of results returned.
* **reverse**: Boolean flag to indicate the result ordering, based on the score. If true, the order is from greater to smaller. Defaults to false.

Sample Response when calling `/categories?q=score&limit=3&reverse=true`
```json
[
    {
        "category": "landline_internet",
        "score": 8
    },
    {
        "category": "race_cars",
        "score": 7
    },
    {
        "category": "stock_investment",
        "score": 6.5
    }
]
```

### GET /products
The endpoint receives three parameters:
* **q**: the type of query to be run. For this exercise it only supports the value "score". This query returns a list of products, alongside their score, calculated as the mean of all the scores on that product, given by the users.
* **limit**: the limit in the number of results returned.
* **reverse**: Boolean flag to indicate the result ordering, based on the score. If true, the order is from greater to smaller. Defaults to false.

Sample Response `/products?q=score&limit=2`
```json
[
    {
        "product": {
            "product_id": "asdf102938",
            "name": "Awesome Programming book",
            "category": "software_engineering"
        },
        "score": 8.5
    },
    {
        "product": {
            "product_id": "5678efgh",
            "name": "Internet Cable Connection, up to 600Mb",
            "category": "landline_internet"
        },
        "score": 9.3
    }
]
```
## Evaluation Notes
* You should submit a working application.
* The application should be production-ready. Feel free to provide the means to pack it on a container structure of your choice.
* You should handle all possible errors derived by your data constraints.
* Bonus points if you provide telemetry.
* Bonus points if you provide an architecture diagram, using the services of a Cloud Provider of your choice.
